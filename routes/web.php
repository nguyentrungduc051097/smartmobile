<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/home', function () {
    return view('home');
});

// List categories

Route::prefix('categories')->group(function () {
    Route::get('/listCategory', [
        'as' => 'categories.list',
        'uses' => 'CategoryController@getListProduct'
    ]);
    Route::get('/index', [
        'as' => 'categories.index',
        'uses' => 'CategoryController@index'
    ]);
    // Route::post('/create', [
    //     'as' => 'categories.create',
    //     'uses' => 'CategoryController@create'
    // ]);

    Route::post('/create', 'CategoryController@create');

    Route::post('/store', [
        'as' => 'categories.store',
        'uses' => 'CategoryController@store'
    ]);
    Route::get('/edit/{id}', [
        'as' => 'categories.edit',
        'uses' => 'CategoryController@edit'
    ]);
    Route::post('/update/{id}', [
        'as' => 'categories.update',
        'uses' => 'CategoryController@update'
    ]);
    Route::get('/delete/{id}', [
        'as' => 'categories.delete',
        'uses' => 'CategoryController@delete'
    ]);
});

// Menu
Route::prefix('menu')->group(function () {
    Route::get('listMenu', 'Menu\MenuController@listMenu');



    Route::get('/index', [
        'as' => 'menu.index',
        'uses' => 'Menu\MenuController@index'
    ]);
    Route::post('/create', [
        'as' => 'menu.create',
        'uses' => 'Menu\MenuController@create'
    ]);
    Route::post('/store', [
        'as' => 'menu.store',
        'uses' => 'Menu\MenuController@store'
    ]);
    Route::post('/edit/{id}', [
        'as' => 'menu.edit',
        'uses' => 'Menu\MenuController@edit'
    ]);
    Route::post('/update/{id}', [
        'as' => 'menu.update',
        'uses' => 'Menu\MenuController@update'
    ]);
    Route::delete('/delete/{id}', [
        'as' => 'menu.delete',
        'uses' => 'Menu\MenuController@delete'
    ]);
});

// Product
Route::prefix('product')->group(function () {
    Route::get('listProduct','Product\ProductController@listProduct');





    Route::get('/index', [
        'as' => 'product.index',
        'uses' => 'Product\ProductController@index'
    ]);
    Route::post('/creat', [
        'as' => 'product.creat',
        'uses' => 'Product\ProductController@creat'
    ]);
    Route::post('/store', [
        'as' => 'product.store',
        'uses' => 'Product\ProductController@store'
    ]);
    Route::post('/edit/{id}', [
        'as' => 'product.edit',
        'uses' => 'Product\ProductController@edit'
    ]);
    Route::post('/update/{id}', [
        'as' => 'product.update',
        'uses' => 'Product\ProductController@update'
    ]);
    Route::get('/delete/{id}', [
        'as' => 'product.delete',
        'uses' => 'Product\ProductController@delete'
    ]);

});


Route::get('list', 'RenderController@renderUser');
Route::get('menu', 'RenderController@listMenu');
Route::get('product','RenderController@listProduct');


