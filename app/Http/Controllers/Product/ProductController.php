<?php

namespace App\Http\Controllers\Product;

use App\Category;
use App\Components\Recusive;
use App\Http\Controllers\Controller;
use App\Product;
use App\ProductImage;
use App\ProductTag;
use App\Tag;
use App\Traits\StorageImageTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{


    use StorageImageTrait;

    private $category;
    private $product;
    private $productImage;

    public function __construct(Category $category, Product $product, ProductImage $productImage, Tag $tag, ProductTag $productTag)
    {
        $this->category = $category;
        $this->product = $product;
        $this->tag = $tag;
        $this->productTag = $productTag;
//        $this->productImage->$productImage;

    }

    public function listProduct()
    {
        $products = $this->product->all();
        return response()->json($products);
    }


    public function index()
    {
        $products = $this->product->latest()->paginate(6);
        return view('Admin.Product.index', compact('products'));
    }

    public function creat(Request $request)
    {
//        try {
            $this->product->create([
                    'name' => $request->name,
                    'price' => $request->price,
                    'content' => $request->contents,
                    'user_id' => auth()->id(),
                    'category_id' => $request->category_id,
                ]
            );
            return response()->json(["status" => true]);
//        } catch (\Throwable $th) {
//            return response()->json(["errMsgerrMsg" => "Lỗi hệ thống"], 422);
//        }
    }

    public function getCategory($parentId)
    {
        $data = $this->category->all();
        $recusive = new Recusive($data);
        $htmlOption = $recusive->categoeyRecusive($parentId);
        return $htmlOption;
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $dataProductCreate = [
                'name' => $request->name,
                'price' => $request->price,
                'content' => $request->contents,
                'user_id' => auth()->id(),
                'category_id' => $request->category_id,
            ];
            $dataUpload = $this->storageTraitUpload($request, 'feature_image_path', 'product');
            if (!empty($dataUpload)) {
                $dataProductCreate['feature_image_name'] = $dataUpload['file_name'];
                $dataProductCreate['feature_image_path'] = $dataUpload['file_path'];
            }
            $product = $this->product->create($dataProductCreate);

            //Insert data to product_image
            if ($request->hasFile('image_path')) {
                foreach ($request->image_path as $fileItem) {
                    $dataProductImageDetail = $this->storageTraitUploadMutiple($fileItem, 'product');
//                dd($dataProductImageDetail);
                    $product->images()->create([
                        'product_id' => $product->id,
                        'image_path' => $dataProductImageDetail['file_path'],
                        'image_name' => $dataProductImageDetail['file_name']
                    ]);

                }
            }

            // Insert Tags
            if (!empty($request->tags)) {
                foreach ($request->tags as $tagItem) {
                    $tagInstance = $this->tag->firstOrCreate(['name' => $tagItem]);
                    $tagId[] = $tagInstance->id;
                }
            }
            $product->tags()->attach($tagId);
            DB::commit();
            return redirect()->route('product.index');

        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error('Message:' . $exception->getMessage() . 'Line:' . $exception->getLine());
        }

    }

    //edit
    public function edit($id)
    {
        $editProduct = $this->product->find($id);
        $htmlOption = $this->getCategory($editProduct->category_id);
        return view('Admin.Product.edit', compact('htmlOption', 'editProduct'));


    }

    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $dataProductUpdate = [
                'name' => $request->name,
                'price' => $request->price,
                'content' => $request->contents,
                'user_id' => auth()->id(),
                'category_id' => $request->category_id,
            ];
            $dataUpload = $this->storageTraitUpload($request, 'feature_image_path', 'product');
            if (!empty($dataUpload)) {
                $dataProductUpdate['feature_image_name'] = $dataUpload['file_name'];
                $dataProductUpdate['feature_image_path'] = $dataUpload['file_path'];
            }
            $this->product->find($id)->update($dataProductUpdate);
            $product = $this->product->find($id);

            //Insert data to product_image
            if ($request->hasFile('image_path')) {
                $this->productImage->where('product_id', $id)->delete();
                foreach ($request->image_path as $fileItem) {
                    $dataProductImageDetail = $this->storageTraitUploadMutiple($fileItem, 'product');
//                dd($dataProductImageDetail);
                    $product->images()->update([
                        'product_id' => $product->id,
                        'image_path' => $dataProductImageDetail['file_path'],
                        'image_name' => $dataProductImageDetail['file_name']
                    ]);

                }
            }

            // Insert Tags
            if (!empty($request->tags)) {
                foreach ($request->tags as $tagItem) {
                    $tagInstance = $this->tag->firstOrCreate(['name' => $tagItem]);
                    $tagId[] = $tagInstance->id;
                }
            }
            $product->tags()->sync($tagId);
            DB::commit();
            return redirect()->route('product.index');

        } catch (\Exception $exception) {
            DB::rollBack();
            Log::error('Message:' . $exception->getMessage() . 'Line:' . $exception->getLine());
        }

    }

    public function delete($id)
    {
        $this->product->find($id)->delete();
        return redirect()->route('product.index');
    }
}
