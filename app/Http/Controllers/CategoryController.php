<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use App\Components\Recusive;
use Validator; 
class CategoryController extends Controller{
    private $category;

    public function __construct(Category $category){
        $this->category = $category;

    }
    public function getListProduct(){
        $categories = $this->category->all();
        return response()->json($categories);

    }

    public function index(){
        $categories = $this->category->latest()->paginate(6);
        return view('Admin.category.index', compact('categories'));
//        return response()->json($categories);
    }


    public function create(Request $request){

      
        try {
              $this->category->create([
            'name' => $request->name,
            'parent_id' => $request->parent_id,
            'slug' => str_slug($request->name)
        ]);

        return response()->json(["status"=>true]);
            //code...
        } catch (\Throwable $th) {
        return response()->json(["errMsgerrMsg"=>"Lỗi hệ thống"], 422);
        }
    }

    public function store(Request $request){


        $this->category->create([
            'name' => $request->name,
            'parent_id' => $request->parent_id,
            'slug' => str_slug($request->name)
        ]);
        // return redirect()->route('categories.index');
    }

    public function getCategory($parentId){
        $data = $this->category->all();
        $recusive = new Recusive($data);
        $htmlOption = $recusive->categoeyRecusive($parentId);
        return $htmlOption;
    }

    public function edit($id){
        $category = $this->category->find($id);
        return response()->json($category);
    }

    public function update(Request $request, $id){
     
        $this->category->find($id)->update([
            'name' => $request->name,
            'parent_id' => $request->parent_id,
            'slug' => str_slug($request->name)
        ]);

        return response()->json(["status"=>true]);
            //code...
       

    }

    public function delete($id){
        $this->category->find($id)->delete();
        return redirect()->route('categories.index');
    }
}
