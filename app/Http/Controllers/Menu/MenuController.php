<?php

namespace App\Http\Controllers\Menu;

use App\Components\MenuRecusive;
use App\Http\Controllers\Controller;
use App\Menu;
use http\Env\Response;
use Illuminate\Http\Request;

class MenuController extends Controller{
    private $menuRecusive;
    private $menu;

    public function __construct(MenuRecusive $menuRecusive, Menu $menu){
        $this->menuRecusive = $menuRecusive;
        $this->menu = $menu;

    }

    public function listMenu(){
        $menus = $this->menu->all();
        return response()->json($menus);
    }

    public function create(Request $request){
        try {
            $this->menu->create([
                'name'=>$request->name,
                'parent_id'=>$request->parent_id,
                'slug'=>str_slug($request->name)

            ]);
            return response()->json(["status => true"]);

        }catch (\Throwable $th){
            return Response()->json([
                "errMsgerrMsg"=>"lỗi hệ thống"
            ],422);

        }




    }

    public function index(){
        $menus= $this->menu->paginate(6);
        return view('Admin.Menu.index',compact('menus'));
    }

//    public function create(){
//        $optionSelect = $this->menuRecusive->menuRecusiveAdd();
//        return view('Admin.Menu.add', compact('optionSelect'));
//    }

    public function store(Request $request){
        $this->menu->create([
            'name'=>$request->name,
            'parent_id'=>$request->parent_id,
            'slug'=>str_slug($request->name)
        ]);
//        return redirect()->route('menu.index');

    }

    public function edit($id){
        $menuFollowEdit= $this->menu->find($id);
        $optionSelect = $this->menuRecusive->menuRecusiveEdit($menuFollowEdit->parent_id);
//        return view('Admin.Menu.edit',compact('optionSelect','menuFollowEdit'));
        return response()->json($menuFollowEdit);
    }

    public function update(Request  $request,$id){
        $this->menu->find($id)->update(
            [
                'name'=>$request->name,
                'parent_id'=>$request->parent_id,
                'slug'=>str_slug($request->name)
            ]
        );
//        return redirect()->route('menu.index');
        return response()->json(["status"=>true]);
    }

    public function delete($id){
       $delete =  $this->menu->find($id)->delete();
        return response()->json($delete);
//        return redirect()->route('menu.index');
    }

}
