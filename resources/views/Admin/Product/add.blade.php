<!-- Stored in resources/views/child.blade.php -->

@extends('layout.admin')

@section('css')
    <link href="{{asset('vendors/select2/select2.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('admin/product/add/add.css')}}" rel="stylesheet"/>

@endsection
@section('title')
    <title>Trang chủ</title>

@endsection


@section('content')
    <<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        @include('partials.content-header', ['name'=>'Product','key'=>'Add'])
        <form action="{{route('product.store')}}" method="post" enctype="multipart/form-data">
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">

                            @csrf
                            <div class="form-group">
                                <label>Tên sản phẩm</label>
                                <input type="text" class="form-control" name="name" placeholder="Tên sản phẩm"
                                       class="form-text text-muted">
                            </div>
                            <div class="form-group">
                                <label>Giá </label>
                                <input type="text" class="form-control" name="price" placeholder="Giá"
                                       class="form-text text-muted">
                            </div>
                            <div class="form-group">
                                <label>Hình ảnh</label>
                                <input type="file" class="form-control-file" name="feature_image_path"
                                       placeholder="Hình ảnh"
                                       class="form-text text-muted">
                            </div>
                            <div class="form-group">
                                <label>Hình ảnh chi tiết</label>
                                <input type="file" multiple class="form-control-file" name="image_path[]"
                                       placeholder="Hình ảnh"
                                       class="form-text text-muted">
                            </div>
                            <div class="form-group">
                                <labell>Danh mục</labell>
                                <select class="form-control select2_init" id="exampleFormControlSelect1"
                                        name="category_id">
                                    <option value="">Chọn danh mục cha</option>
                                    {!! $htmlOption !!}
                                </select>
                            </div>
                            <div class="form-group">
                                <labell>Nhập tags cho sản phẩm</labell>
                                <select class="form-control tags_select_choose" multiple="multiple" name="tags[]">
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Mô tả sản phẩm</label>
                                <textarea class="form-control tinymce_editor_init" name="contents" id="" cols="30"
                                          rows="5   "></textarea>
                            </div>
                            <div class="col-md-12">
                            </div>
                            <button type="submit" class="btn btn-primary"> Thêm</button>
                        </div>
                    </div>
                </div>


            </div>
        </form>
    </div>
    <!-- /.content-wrapper -->
@endsection
@section('js')
    <script src="{{asset('vendors/select2/select2.min.js')}}"></script>
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script src="{{asset('admin/product/add/add.js')}}"></script>
@endsection

