@extends('layout.admin')
@section('title')
    <title>Trang chủ</title>
@endsection
@section('js')
    <script src="{{ asset('/js/factory/services/productService.js') }}"></script>
    <script src="{{ asset('/js/ctrl/ProductCtrl.js') }}"></script>
@endsection
@section('css')
    {{--    <link rel="stylesheet" href="{{asset('admin/product/index/list.css')}}">--}}
@endsection
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        @include('partials.content-header',['name'=>'Product','key'=>'List'])
        <div class="content">
            <div class="table" ng-controller="productCtrl">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <button ng-click="action.showInsertModal()" type="button" class="btn btn-primary"
                                    data-toggle="modal" data-target="#exampleModal">
                                Thêm mới
                            </button>
                            @include('Admin.modal.productModal')
                        </div>
                        <div class="col-md-12">
                            <table class="table table-dark">
                                <thead>
                                <tr>
                                    <th scope="col">STT</th>
                                    <th scope="col">Tên sản phẩm</th>
                                    <th scope="col">Giá</th>
                                    <th scope="col">Hình ảnh</th>
                                    <th scope="col">Danh mục</th>
                                    <th scope="col">Mô tả</th>
                                    <th scope="col">Tác vụ</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="listProduct in dataProduct">
                                    <th scope="row">@{{listProduct.id}}</th>
                                    <td>@{{listProduct.name}}</td>
                                    <td>@{{ listProduct.price}}</td>
                                    <td>
                                        {{--<img class="imageProduct" src="@{{asset(listProduct.feature_image_path)}}"/>--}}
                                    </td>
                                    <td>@{{listProduct.category_id}}</td>
                                    <td>@{{listProduct.content}}</td>
                                    <td>
                                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                                data-target="#exampleModal"
                                                ng-click="action.detailMenu(listProduct.id)">
                                            Sửa
                                        </button>
                                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                                data-target="#delete"
                                                ng-click="action.deleteMenu(listProduct.id)">
                                            xóa
                                        </button>
                                    </td>

                                </tr>

                                </tbody>
                            </table>
                            {{--                        {{$products->links()}}--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.content-wrapper -->
@endsection

