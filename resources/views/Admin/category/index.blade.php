@extends('layout.admin')
@section('title')
    <title>Trang chủ</title>
@endsection
@section('js')
    <script src="{{ asset('/js/factory/services/categoryService.js') }}"></script>
    <script src="{{ asset('/js/ctrl/CategoryCtl.js') }}"></script>
@endsection
@section('content')

    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        @include('partials.content-header',['name'=>'category','key'=>'List'])
        <div class="content">
            <div class="table" ng-controller="CategoryController">
                <table class="table">
                    <!-- Button trigger modal -->
                    <button ng-click="action.showInsertModal()" type="button" class="btn btn-primary"
                            data-toggle="modal" data-target="#exampleModal">
                        Thêm mới
                    </button>
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel"
                         aria-hidden="true" aria-inval id="addModal">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Thêm</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="card-body">
                                        <form action="{{url('categories/store')}}" method="post" id="editForm">
                                            @csrf
                                            <div class="form-group">
                                                <label for="name">Tên danh mục</label>
                                                <input class="form-control" type="text" name="name" id="name"
                                                       ng-model="data.name">
                                            </div>
                                            <div class="form-group">
                                                <labell>Danh mục cha @{{ data.parent_id }}</labell>
                                                <select class="form-control" ng-model="data.parent_id">
                                                    <option value="0">Chọn danh mục cha</option>
                                                    <option ng-value=" listCategory.id  "
                                                            ng-repeat="listCategory in dataCategory">
                                                        @{{ listCategory.name }}
                                                    </option>

                                                    {{--<option value=""><% listCategory.name %></option>--}}
                                                </select>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                                    <button type="button" class="btn btn-primary" ng-click="action.update()">Lưu
                                        thông
                                        tin
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">STT</th>
                        <th scope="col">Tên danh mục</th>
                        <th scope="col">Tác vụ</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="listCategory in dataCategory">
                        <td scope="row">@{{listCategory.id }}</td>
                        <td scope="row">@{{listCategory.name}}</td>

                        <td>
                            <button type="button" class="btn btn-primary" data-toggle="modal"
                                    data-target="#exampleModal"
                                    ng-click="action.detailCategory(listCategory.id)">
                                Sửa
                            </button>
                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#delete"
                                    ng-click="action.deleteUser(listUser.id)">
                                xóa
                            </button>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <!-- Modal Delete-->
                <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                     aria-hidden="true" aria-inval id="addModal">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Xóa</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="card-body">
                                    <form action="" method="post" id="editForm">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <div class="modal-header bg-danger">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title text-center">Bấm xóa để hoàn tất xóa dữ liệu!</h4>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="modal-footer">

                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                                <button type="button" class="btn btn-primary" ng-click="action.confirmDeleteUser()">Xóa
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.content-wrapper -->
@endsection

