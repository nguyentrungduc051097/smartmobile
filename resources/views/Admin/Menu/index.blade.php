@extends('layout.admin')
@section('title')
    <title>Trang chủ</title>
@endsection
@section('js')
    <script src="{{ asset('/js/factory/services/menuService.js') }}"></script>
    <script src="{{ asset('/js/ctrl/MenuCtrl.js') }}"></script>
@endsection
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        @include('partials.content-header',['name'=>'Menu','key'=>'List'])
        <div class="content">
            <div class="table" ng-controller="MenuController">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <button ng-click="action.showInsertModal()" type="button" class="btn btn-primary"
                                    data-toggle="modal" data-target="#exampleModal">
                                Thêm mới
                            </button>
                            @include('Admin.modal.menuModal')
                        </div>
                        <div class="col-md-12">
                            <table class="table table-dark">
                                <thead>
                                <tr>
                                    <th scope="col">STT</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Tác vụ</th>
                                </tr>
                                </thead>


                                <tbody>
                                <tr ng-repeat="listMenu in dataMenu">
                                    <td scope="col">@{{ listMenu.id }}</td>
                                    <td scope="col">@{{ listMenu.name }}</td>
                                    <td>
                                        <button type="button" class="btn-primary" data-toggle="modal"
                                                data-target="#exampleModal"
                                                ng-click="action.detailMenu(listMenu.id)">
                                            Sửa
                                        </button>
                                        <button type="button" class="btn-success" data-toggle="modal"
                                                data-target="#delete"
                                                ng-click="action.deleteMenu(listMenu.id)">
                                            xóa
                                        </button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>

                </div>

            </div>


        </div>
    </div>
    <!-- /.content-wrapper -->
@endsection

