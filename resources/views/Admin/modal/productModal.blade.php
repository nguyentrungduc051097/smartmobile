<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel"
     aria-hidden="true" aria-inval id="addModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Thêm</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card-body">
                    <form action="{{url('menu/store')}}" method="post" id="editForm">
                        @csrf
                        <div class="form-group">
                            <label for="name">Tên sản phẩm</label>
                            <input class="form-control" type="text" name="name" id="name"
                                   ng-model="data.name">
                            <label for="name">Giá </label>
                            <input class="form-control" type="text" name="name" id="name"
                                   ng-model="data.price">
                            <label for="name">Hình ảnh </label>
                            <input class="form-control" type="text" name="name" id="name"
                                   ng-model="data.parent_id">
                            <label for="name">Danh mục</label>
                            <input class="form-control" type="text" name="name" id="name"
                                   ng-model="data.parent_id">
                            <label for="name">Mô tả</label>
                            <input class="form-control" type="text" name="name" id="name"
                                   ng-model="data.content">
                        </div>
                    </form>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                <button type="button" class="btn btn-primary" ng-click="action.update()">Lưu
                    thông
                    tin
                </button>
            </div>
        </div>
    </div>
