app.factory('$menuService', function ($rootScope, $http) {
    let service = {
        action: {},
        data: {}
    };
    //data
    service.data.insertMenu = function (name, parent_id) {
        return {
            name: name || "",
            parent_id: parent_id || ""
        }
    }
    //action

    service.action.listMenu = function () {
        let url = siteUrl + '/menu/listMenu';
        return $http.get(url);
    }
    service.action.insertMenu = function (data) {
        let url = siteUrl + '/menu/create';
        return $http.post(url, data);
    }
    service.action.updateMenu = function (id, data) {
        let url = siteUrl + '/menu/update/' + id;
        return $http.post(url, data);
    }
    service.action.detailMenu = function (id) {
        let url = siteUrl + '/menu/edit/' + id;
        return $http.post(url);
    }
    service.action.deleteMenu = function (id) {
        let url = siteUrl + '/menu/delete/' + id;
        return $http.delete(url);
    }

    return service;
});
