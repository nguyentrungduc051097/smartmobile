app.factory('$categoryService', function ($rootScope, $http) {
    var service = {
        action: {},
        data: {}
    };
    //data
    service.data.insertCategory = function (name, parent_id) {
        return {
            name: name || "",
            parent_id: parent_id || ""
        }
    }
    //action

    service.action.listCategory = function () {
            var url = siteUrl + '/categories/listCategory';
        return $http.get(url);
    }

    service.action.detailCategory = function (id) {
        var url = siteUrl + '/categories/edit/' + id;
        return $http.get(url);
    }
    service.action.insertCategory = function (data) {
        var url = siteUrl + '/categories/create';
        return $http.post(url, data);
    }
    service.action.updateCategory = function (id,data) {
        var url = siteUrl + '/categories/update/' + id;
        return $http.post(url, data);
    }

    return service;
});
