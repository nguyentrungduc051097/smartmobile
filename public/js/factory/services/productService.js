app.factory('$productService', function ($rootScope, $http) {
    let service = {
        action: {},
        data: {}

    };
    //data
    service.data.insertProduct = function (name, price, feature_image_path, content, category_id) {
        return {
            name: name || "",
            price: price || "",
            feature_image_path: feature_image_path || "",
            category_id: category_id || ""

        }
    }

    //action

    service.action.listProduct = function () {
        let url = siteUrl + '/product/listProduct';
        return $http.get(url);
    }
    service.action.insertProduct = function (data) {
        let url = siteUrl + '/product/create';
        return $http.post(url, data);
    }
    return service;
})
