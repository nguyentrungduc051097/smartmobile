app.controller('productCtrl', function ($scope, $http, $productService) {
    $scope.dataProduct = [];
    $scope.data = {
        name: "",
        price: "",
        content: "",
        feature_image_path: "",
        category_id: "",

    };
    let processData = {
        getListProduct: function () {
            $productService.action.listProduct().then(function (response) {
                $scope.dataProduct = response.data;
                console.log($scope.dataProduct);
            }).catch(function (err) {
                console.log(err);
            })
        },
        currentId: 0,
    };
    $scope.action = {
        showInsertModal() {
            $scope.data.currentId = 0;
        },
        update: function () {
            if ($scope.data.currentId == 0) {
                $scope.action.addProduct();
            } else {
                $scope.action.updateProduct();

            }

        },

        // THêm dữ lieu
        addProduct: function () {
            // parma
            let parma = $productService.data.insertProduct($scope.data.name, $scope.data.price, $scope.data.content, $scope.data.feature_image_path, $scope.data.category_id);
            console.log(parma);
            $productService.action.insertProduct(parma).then(function (resp) {
                console.log(resp);
                processData.getListProduct();

            }).catch(function (err) {
                console.log(err)
            })
        },
        updateProduct: function () {

        }


    }
    processData.getListProduct();
})
