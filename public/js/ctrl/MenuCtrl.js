app.controller('MenuController', function ($scope, $http, $menuService) {
    $scope.dataMenu = [];
    $scope.data = {
        name: "",
        parent_id: "",

    };
    let processData = {
        getListMenu: function () {
            $menuService.action.listMenu().then(function (response) {
                $scope.dataMenu = response.data;
                console.log($scope.dataMenu, 'data');
            }).catch(function (err) {
                console.log(err);
            })

        },
        currentId: 0,

    };
    $scope.action = {
        showInsertModal: function () {
            $scope.data.currentId = 0;

        },

        update: function () {
            if ($scope.data.currentId == 0) {
                $scope.action.addMenu();
            } else {
                $scope.action.updateMenu();
            }
        },
        // THêm dữ liệu
        addMenu: function () {
            //param
            let param = $menuService.data.insertMenu($scope.data.name, $scope.data.parent_id);
            console.log(param);
            $menuService.action.insertMenu(param).then(function (resp) {
                console.log(resp);
                processData.getListMenu();
            }).catch(function (err) {
                console.log(err)
            })
            console.log($scope.data.name)
            console.log($scope.data.parent_id)

        },

        updateMenu: function () {
            let param = $menuService.data.insertMenu($scope.data.name, $scope.data.parent_id);
            console.log(param, 'data update');
            $menuService.action.updateMenu($scope.data.currentId, param).then(function (resp) {
                console.log(resp);
                $scope.data.name = resp.data.name;
                $scope.data.parent_id = resp.data.parent_id;
                processData.getListMenu();
            }).catch(function (err) {
                console.log(err)
            })
        },

        detailMenu: function (id) {
            $scope.data.currentId = id;
            $menuService.action.detailMenu(id).then(function (resp) {
                console.log(resp);
                $scope.data.name = resp.data.name;
                $scope.data.parent_id = resp.data.parent_id;

            }).catch(function (err) {
                console.log(err)
            })
        },

        deleteMenu: function (id) {
            $scope.data.currentId = id;
            $menuService.action.deleteMenu(id).then(function (resp) {
                console.log(resp);
                // bootbox.confirm({
                //     message: "Bạn có đồng ý xóa không ??? ",
                //     buttons: {
                //         confirm: {
                //             label: 'ĐỒng ý',
                //             className: 'btn-success'
                //         },
                //         cancel: {
                //             label: 'No',
                //             className: 'btn-danger'
                //         }
                //     },
                //     callback: function (id) {
                //         console.log('Bạn có đồng ý xóa không: ' + id);
                //     }
                // });
            }).catch(function (err) {
                console.log(err)
            })
        }
    }
    processData.getListMenu();

});
