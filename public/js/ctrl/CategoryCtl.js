app.controller('CategoryController', function ($scope, $http, $categoryService) {
    $scope.dataCategory = [];
    $scope.data = {
        name: "",
        parent_id: "",
        getListCategory: function () {
            $categoryService.action.listCategory().then(function (response) {
                $scope.dataCategory = response.data;
                console.log($scope.dataCategory);
            }).catch(function (err) {
                console.log(err);
            })
        },
        currentId: 0,
    };
    $scope.action = {
        showInsertModal:function() {
            $scope.data.currentId = 0;
        },

        update:function(){
            if ($scope.data.currentId == 0) {
                $scope.action.addCategory();
            }else{
                $scope.action.updateCategory();
            }
        },

        updateCategory:function(){
            var param = $categoryService.data.insertCategory($scope.data.name, $scope.data.parent_id);
            console.log(param,' data update')
            $categoryService.action.updateCategory($scope.data.currentId, param).then(function (resp) {
                console.log(resp);
                $scope.data.name = resp.data.name;
                $scope.data.parent_id = resp.data.parent_id;
                $scope.data.getListCategory();


            }).catch(function (err) {
                console.log(err)
            })  
        },

        detailCategory:function(id){
            $scope.data.currentId = id;

            $categoryService.action.detailCategory(id).then(function (resp) {
                console.log(resp);
                $scope.data.name = resp.data.name;   
                $scope.data.parent_id = resp.data.parent_id;   

            }).catch(function (err) {
                console.log(err)
            })  
        },
        addCategory: function () {
            //param

            var param = $categoryService.data.insertCategory($scope.data.name, $scope.data.parent_id);
            console.log(param);
            $categoryService.action.insertCategory(param).then(function (resp) {
                console.log(resp);
                $scope.data.getListCategory();

            }).catch(function (err) {
                console.log(err)
            })
            console.log($scope.data.name);
            console.log($scope.data.parent_id);
        },
    }
    $scope.data.getListCategory();
});
